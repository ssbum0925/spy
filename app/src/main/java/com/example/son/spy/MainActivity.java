package com.example.son.spy;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button startBtn, finishBtn;
    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startBtn = (Button)findViewById(R.id.start_btn);
        finishBtn = (Button)findViewById(R.id.finish_btn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //takeSnapShots();
                Intent intent = new Intent(getApplicationContext(), CameraService.class);
                intent.setPackage("com.example.son.spy");
                startService(intent);
            }
        });

        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CameraService.class);
                intent.setPackage("com.example.son.spy");
                stopService(intent);
            }
        });

        check();

    }

    /** check android version and get permission when the version is over M **/
    public void check() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.INTERNET
                        }, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.INTERNET
                        }, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            }

        }
    }

}
