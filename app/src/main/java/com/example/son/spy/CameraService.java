package com.example.son.spy;

import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceView;

public class CameraService extends Service {

    private int i = 0;
    private Handler m_handler;
    private Runnable m_handlerTask ;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.d("slog", "onStart()");

        /** to handle UI **/
        m_handler = new Handler();
        m_handlerTask = new Runnable() {
            @Override
            public void run() {
                takeSnapShots();
                Log.d("SPY : ", "CAPTURED_" + i);
                i++;

                /** take picture on every 5 seconds **/
                m_handler.postDelayed(m_handlerTask, 10000);
            }
        };
        m_handlerTask.run();


    }

    @Override
    public void onDestroy() {
        m_handler.removeCallbacks(m_handlerTask);
        Log.d("SPY : ", "FINISH");
    }

    public void takeSnapShots() {

        /** make dummy surfaceview **/
        SurfaceView surface = new SurfaceView(this);

        /** open front camera **/
        Camera camera = Camera.open(findFrontCamera());

        try {
            /** set camera preview at dummy surfaceview **/
            camera.setPreviewDisplay(surface.getHolder());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        camera.startPreview();
        camera.takePicture(null,null,jpegCallback);
    }

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            try {

                /** change picture data to bitmap format **/
                Bitmap bitmap = BitmapFactory.decodeByteArray(data,0, data.length);

                /** rotate picture(90 degree to left) **/
                Matrix m = new Matrix();
                m.setRotate(-90, (float) bitmap.getWidth(), (float) bitmap.getHeight());
                Bitmap rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, false);

                /** save bitmap data **/
                String outUriStr = MediaStore.Images.Media.insertImage(getContentResolver(), rotateBitmap,"Captured Image","Captured Image using Camera.");

                if (outUriStr == null) {
                    Log.d("SampleCapture", "Image insert failed.");
                    return;
                } else {

                    /** save picture in device **/
                    Uri outUri = Uri.parse(outUriStr);
                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,outUri));
                }
            } finally {

                /** stop camera preview and close camera **/
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        }
    };

    private int findFrontCamera() {
        int cameraId = -1;
        int numOfCamera = Camera.getNumberOfCameras();

        for(int i = 0; i < numOfCamera; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            /** when exist front camear **/
            if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = 1;
                break;
            }
        }

        return cameraId;
    }
}
